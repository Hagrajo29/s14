



// one line comment (ctrl + slash)
/*
	multiline comment (ctrl + shift + slash)
*/

console.log("Hello Batch 170!");

/*
	Javascript- we can see the messages/log in the console.

	Browser Consoles are part of the browsers which will allow us to see/log messages, data or information from the programming language
	
		they are easily accessed in the dev tools

	Statements
		instructions, expressions that we add to the programming language to communicate with the computers.

		usually ending with a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of the the line.
	
	Syntax
		set of rules that describes how statements should be made/constructed

		lines/blocks of codes must follow a certain set of rules for it to work properly
*/

console.log("Hannah")
/*
	create three console logs to display your favorite food
		console.log("<yourFavoriteFood>")
*/
/*console.log("Adobo")
console.log("Adobo")
console.log("Adobo")*/

let food1 = "Chicken Wings"

console.log(food1);

/*
	Variables are way to store information or data within the JS.

	to create a variable, we first declare the name of the variable with either let/const keyword:

		let nameOfVariable

	Then, we can initialize(define) the variable with a value or data
		
		let nameOfVariable = <valueOfVariable>
*/

console.log("My favorite food is: " + food1);

let food2 = "Kare Kare";

/*console.log("My favorite food is: " + food1 + " and " + food2);*/

console.log("My favorite food is: " + food1 + " and " + food2);

let food3;

/*
	we can create variables without values, but the variables' content would log undefined.
*/
console.log(food3);

food3 = "Chickenjoy";
/*
	we can update the content of a variable by reassigning the value using an assignment operator (=);

	assignment operator (=) lets us (re)assign values to a variable
*/
console.log(food3)


/*
we cannot create another variable with the same name. It will result into an error.
let food1 = "Flat Tops"

console.log(food1)
*/

// const keyword


/*
const keyword will also allow creation of variable. However, with a const keyword, we create constant variable, which means the data saved in these variables will not chnage, cannot change and should not be changed*/

const pi = 3.1416;

console.log(pi);

/*
pi = "Pizza";

console.log(pi);
*/


/*
we also cannot create a const variable without initialization or assigning its value.

const gravity;

console.log(gravity);
*/

/* 
Mini activity
LET KEYWORD
	- reassgin a new value for food 1 with another favorite food of yours
	- reassgin a new value for food 2 with another favorite food of yours
	- log the values of both variables in the console

	CONST KEYWORD
	- 	create a const variable called sunriseDirection with East as its value
	- 	create a const variable called sunsetDirection with West as its value
	log the values of both variables in the console
*/


food1 = "cassava cake";

food2 = "lechon";

console.log(food1);
console.log(food2);

const sunriseDirection = "East";
const sunsetDirection = "West";

console.log(sunriseDirection);
console.log(sunsetDirection);


/*

Guidelines in creating a JS variable

- we can create a let variable with the let keyword. let variables can be reassigned but not redeclared

- creating a variable has two parts: declaration of the variable name; and the initialization of the variable through the use of assignment operator (=)

- a let variable can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.

- not defined cs Undefined. Not defined error means that the variable is used but not declared. Undefined results from a variable that is used but not initialized.


- We can use const keyword to create variables. Constant variables cannot be declared without initialization. Const varilables values cannot be reassigned


- when creating variable names, start with small caps, thi is because we are avoiding conflict with JS that is named with capital letters

-if the variable would need two words, the naming convention is the use of camelCase. Do not add any space for the variables with words as names.
*/


// Data Types


/*
string
	- are data types which are alphanumerical text. They could be a name, phrase, or even a sentence. We can create stringdata types with a single qoute (') or with a double qoute ("). The text inside the quotqtion marks will be displaed as it is.

	keyword variable = "string data";
*/

console.log("Sample String Data")

/*
	numeric data types
		- are data types which are numeric, are displayed when numbers are not placed in the quotation marks. if mathematical operation needed to be done, numeric data type should be used instead of string data type.


		keyword variable = numeric data;
*/

console.log(0123456789);

console.log(1+1);

// Mini Activity

console.log("Hannah Tapay");
console.log("01/23/2018");
console.log("Misamis Oriental");

console.log(14805);
console.log(9);
console.log(2);